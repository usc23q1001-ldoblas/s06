from abc import ABC, abstractclassmethod

class Person(ABC):
	"""docstring for Person"""
	@abstractclassmethod
	def getFullName(self):
		pass

	def addRequest(self):
		pass

	def checkRequest(self):
		pass

	def addUser(self):
		pass

class Employee(Person):
	def __init__(self, firstName, lastName, email, department):
		super().__init__()
		self.firstName=firstName
		self.lastName=lastName
		self.email=email
		self.department=department

	#setter method
	def set_firstName(self, firstName):
		self.firstName=firstName

	def set_lastname(self, lastName):
		self.lastName=lastName

	def set_email(self, email):
		self.email=email

	def set_department(self, department):
		self.department=department

	#getter method
	def get_firstName(self):
		print(f'First Name: {self.firstName}')

	def get_lastName(self):
		print(f'Last Name: {self.lastName}')

	def get_email(self):
		print(f'Email: {self.email}')

	def get_department(self):
		print(f'Department: {self.department}')

	def getFullName(self):
		fullname=self.firstName+" "+self.lastName
		print(f"{fullname}")
		return fullname

	def addRequest(self):
		return f"Request has been added"

	def checkRequest(self):
		print(f"Request has been checked")		

	def addUser(self):
		return f"User has been added"

	def login(self):
		return f"{self.email} has logged in"

	def logout(self):
		return f"{self.email} has logged out"


class TeamLead(Person):
	def __init__(self, firstName, lastName, email, department):
		super().__init__()
		self.firstName=firstName
		self.lastName=lastName
		self.email=email
		self.department=department
		self.employeeArr=[]
	
	#setter method
	def set_firstName(self, firstName):
		self.firstName=firstName

	def set_lastname(self, lastName):
		self.lastName=lastName

	def set_email(self, email):
		self.email=email

	def set_department(self, department):
		self.department=department

	#getter method
	def get_firstName(self):
		print(f'First Name: {self.firstName}')

	def get_lastName(self):
		print(f'Last Name: {self.lastName}')

	def get_email(self):
		print(f'Email: {self.email}')

	def get_department(self):
		print(f'Department: {self.department}')

	def getFullName(self ):
		fullname=self.firstName+" "+self.lastName
		print(f"{fullname}")
		return fullname

	def addRequest(self):
		return f"Request has been added"

	def checkRequest(self):
		print(f"Request has been checked")
		
	def addUser(self):
		return f"User has been added"

	def login(self):
		return f"{self.email} has logged in"

	def logout(self):
		return f"{self.email} has logged out"

	def addMember(self, Employee):
		self.employeeArr.append(Employee)

	def get_members(self):
		return self.employeeArr

class Admin(Person):
	def __init__(self, firstName, lastName, email, department):
		super().__init__()
		self.firstName=firstName
		self.lastName=lastName
		self.email=email
		self.department=department
	
	#setter method
	def set_firstName(self, firstName):
		self.firstName=firstName

	def set_lastname(self, lastName):
		self.lastName=lastName

	def set_email(self, email):
		self.email=email

	def set_department(self, department):
		self.department=department

	#getter method
	def get_firstName(self):
		print(f'First Name: {self.firstName}')

	def get_lastName(self):
		print(f'Last Name: {self.lastName}')

	def get_email(self):
		print(f'Email: {self.email}')

	def get_department(self):
		print(f'Department: {self.department}')
		

	def getFullName(self ):
		fullname=self.firstName+" "+self.lastName
		print(f"{fullname}")
		return fullname

	def addRequest(self):
		return f"Request has been added"

	def checkRequest(self):
		print(f"Request has been checked")
		

	def addUser(self):
		return f"User has been added"

	def login(self):
		return f"{self.email} has logged in"

	def logout(self):
		return f"{self.email} has logged out"

	def addMember(self):
		return f'New Member has been added'


class Request():
	def __init__(self, name, requester, dateRequested):
		super().__init__()
		self.name=name 
		self.requester=requester
		self.dateRequested=dateRequested
		self.status="uncomplete"

	#setter methods
	def set_status(self, status):
		self.status=status 

	def set_name(self,name):
		self.name=name

	def set_requester(self,requester):
		self.requester=requester

	def set_dateRequested(self, dateRequested):
		self.dateRequested=dateRequested

	#getter methods
	def get_name(self):
		print(f"Name: {self.name}")
		
	def date_requester(self):
		print(f"Requester: {self.requester}")

	def get_dateRequested(self):
		print(f"Date Requested: {self.dateRequested}")

	def get_status(self):
		print(f"Status: {self.status}")

	def updateRequest(self):
		return f'Request has been updated'

	def closeRequest(self):
		return f'Request has been closed'

	def cancelRequest(self):
		return f'Request has been canceled'

emp1 = Employee("Lester John", "Doblas", "lesterdoblas@gmail.com", "IT department")
emp1.getFullName()
employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing") 
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing") 
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales") 
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales") 
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing") 
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales") 
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

assert employee1.getFullName() == "John Doe", "Full name should be John Doe" 
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter" 
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added" 
assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.addMember(employee3) 
teamLead1.addMember(employee4)
for indiv_emp in teamLead1.get_members():
	print(indiv_emp.getFullName())
assert admin1.addUser() == "User has been added"

req2.set_status("closed") 
print(req2.closeRequest())